import React from 'react'; // Importa la biblioteca React
import '../css/registro.css'; // Asegúrate de que la ruta al archivo CSS sea correcta

export const Register = (props) => {// Define un componente funcional llamado "Register" que recibe "props" como argumento.
 // Función para manejar el registro
 const registrar = (event) => {// Define una función llamada "registrar" que toma un evento como argumento.
  event.preventDefault(); // Previene el comportamiento predeterminado del evento (en este caso, evitar que se envíe el formulario).
  
  // Previene el comportamiento predeterminado del evento (en este caso, evitar que se envíe el formulario).
  const usernameInput = document.querySelector('#username');
  const emailInput = document.querySelector('#email');
  const passwordInput = document.querySelector('#password');
  const confirmPasswordInput = document.querySelector('#confirm-password');
  const telefonoInput = document.querySelector('#telefono');

  // Expresión regular para validar el formato del correo electrónico
  const emailFormat = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
  const phonePattern = /^\d{10}$/; // Cambiado para requerir 10 dígitos
  const numberPattern = /\d/; // Al menos un número en la contraseña
   // Validaciones de los campos del formulario
  if (usernameInput.value === '' || emailInput.value === '' || passwordInput.value === '' || confirmPasswordInput.value === '') {
    alert('Por favor, complete todos los campos.'); // Muestra una alerta si hay campos vacíos.
    return false;
  }

  if (!emailFormat.test(emailInput.value)) {
    alert('El correo electrónico no tiene un formato válido.'); // Muestra una alerta si el correo electrónico no tiene un formato válido.
    return false;
  }

  if (!phonePattern.test(telefonoInput.value)) {
    alert('El número celular debe contener exactamente 10 dígitos.');// Muestra una alerta si el número de teléfono no tiene 10 dígitos.
    return false;
  }

  if (!numberPattern.test(passwordInput.value)) {
    alert('La contraseña debe contener al menos un número.');// Muestra una alerta si la contraseña no contiene al menos un número.
    return false;
  }

  if (passwordInput.value !== confirmPasswordInput.value) { // Muestra una alerta si las contraseñas no coinciden.
    alert('Las contraseñas no coinciden.');
    return false;
  }

  // Obtener la lista actual de usuarios registrados o inicializarla si es la primera vez
  const userList = JSON.parse(sessionStorage.getItem('userList')) || [];

  // Agregar el nuevo usuario a la lista
  userList.push({
    username: usernameInput.value,
    email: emailInput.value,
    telefono: telefonoInput.value,
    password: passwordInput.value,
    habitacion: '0',
    rol: 'Estudiante',
  });

  // Almacenar la lista actualizada en sessionStorage
  sessionStorage.setItem('userList', JSON.stringify(userList));

  alert('¡Registrado correctamente!'); // Muestra una alerta de registro exitoso.
  window.location.href = 'login.html'; // Redirige a la página de inicio de sesión con un registro exitoso
};

  return (
    <div>
    <header>
      <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />


        <p className="uleam-text">ULEAM</p>
      </div>
    </header>

    <div className="fondo">
      {/* Cuerpo del formulario */}
      <div className="form-container">
      <form className="register-form" onSubmit={registrar}>
          <h2>REGISTRO</h2>
          <div className="separador">
            <input required type="text" id="username" className="input" />
            <label>{/* Etiqueta con efecto de animación para el campo de entrada de usuario. */}
              <span style={{ transitionDelay: '0ms', left: '0px' }}>U</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>s</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>u</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>a</span>
              <span style={{ transitionDelay: '300ms', left: '60px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '72px' }}>i</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Usuario</p>
            </label>{/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
          </div>

          <div className="separador">
            <input required type="text" id="nombres" className="input" />
            <label>{/* Etiqueta con efecto de animación para el campo de entrada de nombre. */}
              <span style={{ transitionDelay: '0ms', left: '0px' }}>N</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>m</span>
              <span style={{ transitionDelay: '225ms', left: '57px' }}>b</span>
              <span style={{ transitionDelay: '300ms', left: '71px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '80px' }}>e</span>
              <span style={{ transitionDelay: '450ms', left: '95px' }}>s</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Nombres</p>
            </label>{/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
          </div>
          <div className="separador">
            <input required type="text" id="apellidos" className="input" />
            <label>{/* Etiqueta con efecto de animación para el campo de entrada de apellido. */}
              <span style={{ transitionDelay: '0ms', left: '0px' }}>A</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>p</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>e</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>l</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>l</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>i</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>d</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>o</span>
              <span style={{ transitionDelay: '600ms', left: '115px' }}>s</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Apellidos</p>
            </label>{/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
          </div>
          <div className="separador">
            <input required type="text" id="telefono" className="input" />
            <label>{/* Etiqueta con efecto de animación para el campo de entrada de telefono. */}
              <span style={{ transitionDelay: '0ms', left: '0px' }}>T</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>e</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>l</span>
              <span style={{ transitionDelay: '225ms', left: '40px' }}>e</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>f</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>o</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>n</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Telefono</p>
            </label>{/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
          </div>
          <div className="separador">
            <input required type="text" id="email" name="email" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '34px' }}>r</span>
              <span style={{ transitionDelay: '225ms', left: '43px' }}>r</span>
              <span style={{ transitionDelay: '300ms', left: '50px' }}>e</span>
              <span style={{ transitionDelay: '375ms', left: '65px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Correo</p>
            </label>
          </div>
          <div className="separador">
            <input required type="password" id="password" name="password" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{ transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{ transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Contraseña</p>
            </label>
          </div>
          <div className="separador">
            <input required type="password" id="confirm-password" name="confirm-password" className="input" />
            <label>
              <span style={{ transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{ transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{ transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Confirmar</p>
            </label>
          </div>
          <div className="terms">
            <input type="checkbox" id="accept-terms" tabIndex="5" required />
            <label htmlFor="accept-terms">Acepto los Términos y Condiciones</label>
          </div>
          <span className="link-text">¿Ya tienes una cuenta?{' '}
          <span className="link-clic" onClick={() => props.onFormSwitch('login')}>Inicia sesión aquí</span>.</span>
          <button type="submit" tabIndex="6" className="btn">Registrarse</button> {/* Botón para enviar el formulario y registrarse.*/}
        </form>
      </div>
    </div>

    {/* Pie de página */}
    <footer className="footer">
      <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
    </footer>
  </div>
  );
};
