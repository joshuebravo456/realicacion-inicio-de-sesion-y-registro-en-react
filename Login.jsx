import React from "react"; // Importa la biblioteca React
import '../css/login.css'; // Importa el archivo CSS para estilos. Asegúrate de que la ruta al archivo CSS sea correcta.

export const Login = (props) => { // Define un componente funcional llamado "Login" que recibe "props" como argumento.
  const iniciarSesion = (event) => { // Define una función llamada "iniciarSesion" que toma un evento como argumento.
    event.preventDefault(); // Previene el comportamiento predeterminado del evento (en este caso, evitar que se envíe el formulario).

    // Obtener los valores ingresados en los campos de entrada
    const usernameInput = document.querySelector('#login-username');
    const passwordInput = document.querySelector('#login-password');

    // Obtener la lista de usuarios registrados desde sessionStorage
    const userList = JSON.parse(sessionStorage.getItem('userList')) || [];

    if (userList.length === 0) {
      alert('No hay usuarios registrados. Regístrese primero.');
      return false;
    }

    // Realizar la búsqueda en la lista de usuarios registrados
    const foundUser = userList.find((user) => user.username === usernameInput.value && user.password === passwordInput.value);

    if (foundUser) { // Comprueba si se encontró un usuario.
      alert('¡Sesión iniciada correctamente!'); // Muestra una alerta indicando que la sesión se inició correctamente.
      window.location.href = 'habitacion.html'; // Redirige a la página de habitación si las credenciales son correctas
    } else {
      alert('Credenciales incorrectas. Por favor, inténtelo de nuevo.');// Muestra una alerta indicando que las credenciales 
      //son incorrectas y pide que se intente nuevamente.
    }
  };


  return (
    <div>
      <header>
        <div className="logo">
        <img src={process.env.PUBLIC_URL + '/eloy.png'} alt="Logo" />
          <p className="uleam-text">ULEAM</p>
        </div>
    
      </header>

      <div className="fondo">
        {/* Cuerpo del formulario */}
        <form className="form-login" onSubmit={iniciarSesion}>
          <h2>Iniciar sesión</h2>

          <div className="separador">
            <input required type="text" id="login-username" name="login-username" className="input" /> 
            <label> {/* Etiqueta con efecto de animación para el campo de entrada de contraseña. */}
              <span style={{ transitionDelay: '0ms', left: '0px' }}>U</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>s</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>u</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>a</span>
              <span style={{ transitionDelay: '300ms', left: '60px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '72px' }}>i</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>o</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Usuario</p>
            </label> {/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
          </div>
          <div className="separador">
            <input required type="password" id="login-password" name="login-password" className="input" />
            <label>{/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
              <span style={{ transitionDelay: '0ms', left: '0px' }}>C</span>
              <span style={{ transitionDelay: '75ms', left: '20px' }}>o</span>
              <span style={{ transitionDelay: '150ms', left: '33px' }}>n</span>
              <span style={{ transitionDelay: '225ms', left: '47px' }}>t</span>
              <span style={{ transitionDelay: '300ms', left: '56px' }}>r</span>
              <span style={{ transitionDelay: '375ms', left: '68px' }}>a</span>
              <span style={{ transitionDelay: '450ms', left: '85px' }}>s</span>
              <span style={{ transitionDelay: '525ms', left: '100px' }}>e</span>
              <span style={{ transitionDelay: '600ms', left: '115px' }}>ñ</span>
              <span style={{ transitionDelay: '712ms', left: '135px' }}>a</span>
              <p style={{ position: 'absolute', left: '-8px', top: '-10px', fontSize: '24px', margin: '10px', color: 'rgb(73, 73, 73)', transition: '0.5s', pointerEvents: 'none' }}>Contraseña</p>
            </label>{/* Las siguientes líneas de código generan letras animadas en la etiqueta. */}
          </div>
          
          <span className="link-text">¿No tienes una cuenta?{' '}
          <span className="link-clic" onClick={() => props.onFormSwitch('register')}>Regístrate aquí</span>.</span> 
                  <button type="submit" tabIndex="3" className="btn">Iniciar sesión</button>
        </form>
      </div>

      {/* Pie de página */}
      <footer className="footer">
        <strong className="gestion-title">MANTA - MANABÍ - ECUADOR Copyright 2023</strong>
      </footer>
    </div>
  )
};
